# Respeaker

> Respeaker is an application for the microphone array solutions of SEEED, based on librespeaker which combines the audio front-end processing algorithms.

## Installation

To install the respeaker repository you can find a guide [here](installation_guide.md).

### Dotenv

The repository uses an .env file. In the file the ip address of the server is stored along withe the mac address of the ReSpeaker Core v2.0. To get the ip address of the server you have to run another [repository](https://bitbucket.org/qbmt/discovery_tool_respeaker/src/master/). After you've run the repository you can run the following command to put the mac address in the .env file.

```bash
make dotenv
```

### Compile and run

Run the whole project

```bash
make run
```

### Exit

Exiting the running program:
<kbd>CTRL</kbd>+<kbd>Z</kbd>

```bash
kill -9 `jobs -ps`
```

#### Clean

```bash
make clean
```

#### Crashes

When you get following error, you have to restart pulseaudio.

![error]( /img/error.png)

Restart pulseaudio with following commands.

```bash
sudo killall pulseaudio
pulseaudio --start
```

## Hardware

For this project, the [ReSpeaker Core v2.0](http://wiki.seeedstudio.com/ReSpeaker_Core_v2.0/) is used.

## Software

Our software is based on examples of [librespeaker](http://respeaker.io/librespeaker_doc/). It makes use of Snowboy.

## Meta

Frederik Feys – [FrederikFeys](https://github.com/FrederikFeys) – frederik.feys@student.vives.be

Arthur Verstraete - [ArthurVerstraete](https://github.com/ArthurVerstraete) - arthur.verstraete@student.vives.be
