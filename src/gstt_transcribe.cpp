// Copyright 2016 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "gstt_transcribe.h"


void Transcribe::init() {
  int argc = 2;
  const char* arg = "";
  const char** argv = &arg;

  auto creds = grpc::GoogleDefaultCredentials();
  auto channel = grpc::CreateChannel("speech.googleapis.com", creds);
  std::unique_ptr<Speech::Stub> speech(Speech::NewStub(channel));

  StreamingRecognizeRequest configRequest;
  auto* streaming_config = configRequest.mutable_streaming_config();
  RecognitionConfig *recognition_config = new RecognitionConfig();
  streaming_config->set_allocated_config(recognition_config);
  streaming_config->set_single_utterance(true);
  streaming_config->set_interim_results(true);

  ParseArguments(argc, (char**)argv, streaming_config->mutable_config());
  
  streamer = speech->StreamingRecognize(&context);
  // Write the first request, containing the config only.
  streamer->Write(configRequest);
  cout << "wrote config file to google" << endl;
}

void Transcribe::sendDataToGoogle(const char* data, int length) {
  request.set_audio_content(data, length);
  streamer->Write(request);
}


std::vector<std::string> Transcribe::ResponseThreadMain() {
  std::vector<std::string> output;
  // Read responses.
  StreamingRecognizeResponse response;
  while (streamer->Read(&response)) {  // Returns false when no more to read.
    // Dump the transcript of all the results.
    for (int r = 0; r < response.results_size(); ++r) {
      const auto& result = response.results(r);
      std::cout << "Result stability: " << result.stability() << std::endl;
      for (int a = 0; a < result.alternatives_size(); ++a) {
        const auto& alternative = result.alternatives(a);
        std::cout << alternative.confidence() << "\t"
                  << alternative.transcript() << std::endl;
        if (result.stability() == 0) {
          output.push_back(to_string(alternative.confidence()));
          output.push_back(alternative.transcript());
          setStreamEnded(1);
        }
      }
    }
  }
  return output;
}

void Transcribe::setStreamEnded(int se) {
  streamEnded = se;
}

int Transcribe::getStreamEnded() {
  return streamEnded;
}


void Transcribe::endOfStream() {
  streamer->WritesDone();
  grpc::Status status = streamer->Finish();
  if (!status.ok()) {
    // Report the RPC failure.
    std::cerr << status.error_message() << std::endl;
  }
}

