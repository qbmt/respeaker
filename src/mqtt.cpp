#include "mqtt.h"

const std::string LWT_TOPIC				{ "events/disconnect" };
const std::string LWT_PAYLOAD			{ "Last will and testament." };

const int  QOS = 1;
const auto TIMEOUT = std::chrono::seconds(10);

// mqtt::async_client client&;

/////////////////////////////////////////////////////////////////////////////

using namespace std;

MqttClient::MqttClient(std::string clientID, std::string address)
:client(address, clientID){

}

int MqttClient::connectToBroker() {

	cout << "\n#################### Initializing for mqtt-server ####################\n" << endl;

	mqtt::connect_options connopts("", "");
	mqtt::message willmsg(LWT_TOPIC, LWT_PAYLOAD, QOS, true);
	mqtt::will_options will(willmsg);

	connopts.set_will(will);
	connopts.set_keep_alive_interval(30);
	connopts.set_automatic_reconnect(true);
	connopts.set_connect_timeout(5);
    connopts.set_clean_session(true);

	try {
		mqtt::token_ptr conntok = client.connect(connopts);
		
		conntok->wait();
		
	}
	catch (const mqtt::exception& exc) {
		cerr << exc.what() << endl;
	}

 	return 0;
	
}

void MqttClient::triggerRespeakerViaMqtt(std::string topic) {
	client.subscribe(topic, 0);
	client.set_message_callback([this](mqtt::const_message_ptr msg) {
		if(msg->get_payload_str() == "startListening"){	
			setTriggered(true);
		} else if (msg->get_payload_str().substr(0, 8) == "language"){
			ofstream outputfile;
			outputfile.open("./.env.language");

			if(!outputfile){
				cout << "Unable to open .env.language for writing" << endl;
			}

			outputfile << msg->get_payload_str().substr(9, 5);
			outputfile.close();
		}
	});
}

void MqttClient::setTriggered(bool triggered){
	triggeredViaMqtt = triggered;
}

bool MqttClient::getTriggered(){
	return triggeredViaMqtt;
}


bool MqttClient::publish(std::string topic, std::string message) {

	if (!client.is_connected()) {
		cout << "MQTT Client is not connected" << endl;
		return false;
	}

	try {
		
		auto msg = mqtt::make_message(topic, message, QOS, false);
		if (!client.publish(msg)->wait_for(TIMEOUT)) {
			cout << "Publish timed out" << endl;
		}
	}
	catch (const mqtt::exception& exc) {
		cerr << exc.what() << endl;
		return false;
	}

 	return true;
}


bool MqttClient::isConnected() {

	return client.is_connected();

}

