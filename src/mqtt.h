#pragma once

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <chrono>
#include <cstring>
#include "./resources/mqtt/client.h"

class MqttClient {

    public:
        MqttClient(std::string clientID, std::string address);
        void triggerRespeakerViaMqtt(std::string topic);
        bool publish(std::string topic, std::string message);
        int connectToBroker();
        bool isConnected();
        void setTriggered(bool triggered);
        bool getTriggered();
        

    private:
        mqtt::async_client client;
        bool triggeredViaMqtt = false;
};