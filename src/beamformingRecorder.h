#pragma once
#include <string>
#include <ctime>
#include <future>
#include <vector>
#include "./gstt_transcribe.h"
#include <respeaker.h>
#include <chain_nodes/pulse_collector_node.h>
#include <chain_nodes/vep_aec_beamforming_node.h>
#include <chain_nodes/snowboy_manual_beam_kws_node.h>

extern "C" {
    #include <sndfile.h>
}

using namespace std;

class BeamformingRecorder {
    private:
        // parse opts
        std::string source;
        bool enable_wav;
        bool enable_agc;
        int agc_level;
        std::string mic_type;
        std::unique_ptr<respeaker::PulseCollectorNode> collector;
        std::unique_ptr<respeaker::VepAecBeamformingNode> vep_bf;
        std::unique_ptr<respeaker::SnowboyManKwsNode> man_kws;
        std::unique_ptr<respeaker::ReSpeaker> respeaker;
        std::string data;
        int frames;
        int hotword_index;
        int hotword_count;
        int min_duration;
        int max_duration;
        std::clock_t start;
        double duration;
        bool recording;
        bool clock;
        bool someoneIsSpeaking;
        bool someoneWasSpeaking;
        bool speech;
        std::string mic;
        SF_INFO sfinfo;
        size_t num_channels;
    public:
        int initializeRespeaker();
        void initializeDefaults();
        void setBeam(std::string mic);
        int record(Transcribe *trans);
        int startBfRec();
        void stopBfRec();
        void pauseBfRec();
        int resumeBfRec();
        bool stop = false;
};