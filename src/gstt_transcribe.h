#pragma once
#include <grpc++/grpc++.h>
#include <strings.h>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>
#include <thread>
#include <future>
#include "google/cloud/speech/v1/cloud_speech.grpc.pb.h"
#include "gstt_parse_arguments.h"

using namespace std;

using google::cloud::speech::v1::Speech;
using google::cloud::speech::v1::StreamingRecognizeRequest;
using google::cloud::speech::v1::StreamingRecognizeResponse;
using google::cloud::speech::v1::RecognitionConfig;

class Transcribe {
    public:
        std::vector<std::string> convert();
        void endOfStream();
        void sendDataToGoogle(const char* data, int length);
        std::vector<std::string> ResponseThreadMain();
        void init();
        void setStreamEnded(int se);
        int getStreamEnded();
    private:
        std::unique_ptr<grpc::ClientReaderWriter<google::cloud::speech::v1::StreamingRecognizeRequest, google::cloud::speech::v1::StreamingRecognizeResponse>> streamer;
        grpc::ClientContext context;
        StreamingRecognizeRequest request;
        int streamEnded = 0;
};