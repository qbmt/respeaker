#include "beamformingRecorder.h"
#include <cstring>
#include <memory>
#include <iostream>
#include <csignal>
#include <thread>

extern "C"
{
    #include <sndfile.h>
    #include <unistd.h>
    #include <getopt.h>
}

using namespace std;
using namespace respeaker;

#define BLOCK_SIZE_MS    8


int BeamformingRecorder::initializeRespeaker() {

    collector.reset(PulseCollectorNode::Create_48Kto16K(source, BLOCK_SIZE_MS));
    vep_bf.reset(VepAecBeamformingNode::Create(StringToMicType(mic_type), false, 6, enable_wav));

    man_kws.reset(SnowboyManKwsNode::Create("./src/resources/common.res",
                                    "./src/resources/snowboy.umdl",
                                    "0.9",
                                    10, 
                                    enable_agc,
                                    true,
                                    false));
    
    man_kws->DisableAutoStateTransfer();

    if (enable_agc) {
        man_kws->SetAgcTargetLevelDbfs(agc_level);
        cout << "\n#################### AGC = -"<< agc_level<< " ####################\n" << endl;
    }
    else {
        cout << "\n#################### Disable AGC ####################\n" << endl;
    }

    vep_bf->Uplink(collector.get());
    man_kws->Uplink(vep_bf.get());
}

void BeamformingRecorder::initializeDefaults() {
    std::string source = "default";
    bool enable_wav = false;
    bool enable_agc = false;
    int agc_level = 10;
    std::string mic_type = "";
    std::string data = "";
    int frames = 0;
    int hotword_index = 0;
    int hotword_count = 0;
    std::clock_t start = 0;
    double duration = 0;
    bool recording = false;
    std::string mic = "";
    SF_INFO sfinfo;
    size_t num_channels = 0;
    clock = true;
    someoneIsSpeaking = true;
    someoneWasSpeaking = true; 
    speech = true;
    min_duration = 1;
    max_duration = 30;
}


int BeamformingRecorder::startBfRec() {
    respeaker.reset(ReSpeaker::Create());
    respeaker->RegisterChainByHead(collector.get());
    respeaker->RegisterOutputNode(man_kws.get());
    respeaker->RegisterDirectionManagerNode(man_kws.get());
    respeaker->RegisterHotwordDetectionNode(man_kws.get());

    if (!respeaker->Start(&stop)) {
        cout << "\n#################### Can not start the respeaker node chain. ####################\n" << endl;
        return -1;
    }

    num_channels = respeaker->GetNumOutputChannels();
    int rate = respeaker->GetNumOutputRate();
    cout << "\n#################### num channels: " << num_channels << ", rate: " << rate << " ####################\n" << endl;

    memset (&sfinfo, 0, sizeof (sfinfo));
    sfinfo.samplerate   = rate;
    sfinfo.channels     = num_channels;
    sfinfo.format       = (SF_FORMAT_WAV | SF_FORMAT_PCM_16);
}

void BeamformingRecorder::stopBfRec() {
    cout << "\n#################### stopping beamformingRecorder... ####################\n" << endl;
    respeaker->Stop();
    cout << "\n#################### cleanup done. ####################\n" << endl;
}

void BeamformingRecorder::pauseBfRec() {
    cout << "\n#################### pausing beamformingRecorder... ####################\n" << endl;
    respeaker->Pause();
}

int BeamformingRecorder::resumeBfRec() {
    cout << "\n#################### resuming beamformingRecorder... ####################\n" << endl;
    respeaker->Resume();
    return 0;
}


void BeamformingRecorder::setBeam(std::string mic) {
    if (mic != "unknown") {
        man_kws->SetBeamNum(stoi(mic));
        cout << "\n#################### Set Beam Number: " << stoi(mic) << " ####################\n" << endl;
    } else {
        cout << "\n#################### ERROR: can not do beam-forming ####################\n" << endl;
    }
}


int BeamformingRecorder::record(Transcribe* trans) {

    cout << "\n#################### start recording ####################\n" << endl;

    std::clock_t startRecord = std::clock();

    while (true) {
        hotword_index = 1;
        data = respeaker->DetectHotword(hotword_index);
        speech = respeaker->GetVad();
    

        if (clock) {
            start = std::clock();
            clock = false;
        }
        
        frames = data.length() / (sizeof(int16_t) * num_channels);
        trans->sendDataToGoogle(data.data(), data.length());
        
        duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
        double durationRecord = ( std::clock() - startRecord ) / (double) CLOCKS_PER_SEC;
        if (trans->getStreamEnded()) {
            cout << "duration of recording: " << durationRecord << endl;
            cout << "\n#################### stop recording ####################\n" << endl;
            trans->endOfStream();
            return 0;
        }

        if (duration >= min_duration) {
            someoneWasSpeaking = someoneIsSpeaking;
            someoneIsSpeaking = speech;
            clock = true;

            cout << "speech: " << speech << ", is: " << someoneIsSpeaking << ", was: " << someoneWasSpeaking << endl;
            
            if (!someoneWasSpeaking && !someoneIsSpeaking) {
                cout << "duration of recording: " << durationRecord << endl;
                cout << "\n#################### stop recording ####################\n" << endl;
                trans->endOfStream();
                return 0;
            }
        }

        if (durationRecord > max_duration) {
            cout << "duration of recording: " << durationRecord << endl;
            cout << "\n#################### stop recording ####################\n" << endl;
            trans->endOfStream();
            return 0;
        }
    }
} 
