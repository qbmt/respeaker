#pragma once
#include <string>
#include <respeaker.h>
#include <chain_nodes/pulse_collector_node.h>
#include <chain_nodes/vep_aec_beamforming_node.h>
#include <chain_nodes/snowboy_1b_doa_kws_node.h>
#include <ctime>

using namespace respeaker;

class KwsDirection {
    private:
        std::string data;
        std::unique_ptr<respeaker::PulseCollectorNode> collector;
        std::unique_ptr<respeaker::VepAecBeamformingNode> vep_1beam;
        std::unique_ptr<respeaker::Snowboy1bDoaKwsNode> snowboy_kws;
        std::unique_ptr<respeaker::ReSpeaker> respeaker;
        // parse opts
        std::string source = "default";
        bool enable_agc = false;
        bool enable_wav = false;
        int agc_level = 10;
        std::string mic_type;
        int hotword_index = 0, hotword_count = 0;
        std::clock_t start;
        std::clock_t clockInFileName;
        double duration;
        bool recording = false;
        std::string mic;
        int frames;
        
    public:
        // KwsDirection();
        int initializeRespeaker();
        void initializeDefaults();
        void detectHotword();
        std::string getMic();
        void stopKwsDir();
        int startKwsDir();
        int resumeKwsDir();
        void pauseKwsDir();
        bool stop = false;
};