// Copyright 2016 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "gstt_parse_arguments.h"
#include <getopt.h>
#include "google/cloud/speech/v1/cloud_speech.pb.h"

using google::cloud::speech::v1::RecognitionConfig;

char* ParseArguments(int argc, char** argv, RecognitionConfig* config) {

  std::string language = "en-US";
  std::string inputline;
  std::ifstream inputfile;
  inputfile.open(".env.language");
  if(!inputfile){
    std::cout << "Unable to open .env.language for reading" << std::endl;
  }
  
  while (getline(inputfile, inputline)) {
    if(inputline == "en-US" || inputline == "en-GB" || inputline == "nl-BE" || inputline == "fr-FR" || inputline == "de-DE") {
      language = inputline;
    }
  } 
  
  config->set_language_code(language);
  config->set_sample_rate_hertz(16000);  // Default sample rate.
  config->set_encoding(RecognitionConfig::LINEAR16);

  return argv[optind];
}
