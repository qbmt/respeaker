#include "kwsDirection.h"
#include <cstring>
#include <memory>
#include <iostream>
#include <csignal>
#include <chrono>
#include <thread>

extern "C"
{
    #include <sndfile.h>
    #include <unistd.h>
    #include <getopt.h>
}
using namespace std;
using namespace respeaker;

#define BLOCK_SIZE_MS    8


int KwsDirection::initializeRespeaker() {

    collector.reset(PulseCollectorNode::Create_48Kto16K(source, BLOCK_SIZE_MS));
    vep_1beam.reset(VepAecBeamformingNode::Create(StringToMicType(mic_type), true, 6, enable_wav));
    // vep_1beam.reset(VepAec1BeamNode::Create(LINEAR_6MIC_8BEAM, 6));
    cout << "\n#################### using snowboy kws ####################\n" << endl;
    snowboy_kws.reset(Snowboy1bDoaKwsNode::Create("./src/resources/common.res",
                                                    "./src/resources/snowboy.umdl",
                                                    // "./src/resources/jarvis.pmdl",
                                                    // "./src/resources/Hey Cedric.pmdl",
                                                    "0.7",
                                                    10,
                                                    enable_agc,
                                                    false));

    snowboy_kws->DisableAutoStateTransfer();

    if (enable_agc) {
        snowboy_kws->SetAgcTargetLevelDbfs(agc_level);
        cout << "\n#################### AGC = -"<< agc_level<< " ####################\n" << endl;
    }
    else {
        cout << "\n#################### Disable AGC ####################\n" << endl;
    }  

    // collector->SetThreadPriority(50);
    // vep_1beam->SetThreadPriority(99);
    // snowboy_kws->SetThreadPriority(51);
    // vep_1beam->BindToCore(1);
    // snowboy_kws->BindToCore(2);

    vep_1beam->Uplink(collector.get());
    snowboy_kws->Uplink(vep_1beam.get());
}

void KwsDirection::initializeDefaults() {
    std::string data = "";
    std::string source = "default";
    bool enable_agc = false;
    bool enable_wav = false;
    int agc_level = 10;
    std::string mic_type = "";
    int hotword_index = 0;
    int hotword_count = 0;
    std::clock_t start = 0;
    std::clock_t clockInFileName = 0;
    double duration = 0;
    bool recording = false;
    std::string mic = "";
    int frames = 0;
}

std::string KwsDirection::getMic() {
    while (true) {
        
        // data = respeaker->DetectHotword(hotword_index);
        hotword_index = respeaker->DetectHotword();

        if (hotword_index >= 1) {
            hotword_count++;
            cout << "\n#################### hotword count: " << hotword_count << ", index: " << hotword_index << " ####################\n" << endl;
            int direction = snowboy_kws->GetDirection();
            cout << "\n#################### Direction of Arrival: " << direction << " ####################\n" << endl;
            if (direction >= 30 && direction < 90) {
                return "1";
            } else if (direction >= 90 && direction < 150) {
                return "2";
            } else if (direction >= 150 && direction < 210) {
                return "3";
            } else if (direction >= 210 && direction < 270) {
                return "4";
            } else if (direction >= 270 && direction < 330) {
                return "5";
            } else if ((direction >= 330 && direction <= 360) | (direction >= 0 && direction < 30)) {
                return "6";
            } else {
                return "unkown";
            }
        }
    }
}

int KwsDirection::startKwsDir() {
    respeaker.reset(ReSpeaker::Create());
    respeaker->RegisterChainByHead(collector.get());
    respeaker->RegisterOutputNode(snowboy_kws.get());
    respeaker->RegisterDirectionManagerNode(snowboy_kws.get());
    respeaker->RegisterHotwordDetectionNode(snowboy_kws.get());
    
    if (!respeaker->Start(&stop)) {
        cout << "\n#################### Can not start the respeaker node chain. ####################\n" << endl;
        return -1;
    } else {
        cout << "\n#################### starting kwsDirection... ####################\n" << endl;
    }
    
    size_t num_channels = respeaker->GetNumOutputChannels();
    int rate = respeaker->GetNumOutputRate();
    cout << "\n#################### num channels: " << num_channels << ", rate: " << rate << " ####################\n" << endl;
    // init libsndfile
    SNDFILE *commandFile;
    SF_INFO sfinfo;

    memset (&sfinfo, 0, sizeof (sfinfo));
    sfinfo.samplerate   = rate ;
    sfinfo.channels     = num_channels ;
    sfinfo.format       = (SF_FORMAT_WAV | SF_FORMAT_PCM_16) ;
}

void KwsDirection::stopKwsDir() {
    cout << "\n#################### stopping kwsDirection... ####################\n" << endl;
    respeaker->Stop();
    cout << "\n#################### cleanup done. ####################\n" << endl;   
}

void KwsDirection::pauseKwsDir() {
    cout << "\n#################### pausing kwsDirection... ####################\n" << endl;
    respeaker->Pause();
}

int KwsDirection::resumeKwsDir() {
    cout << "\n#################### resuming kwsDirection... ####################\n" << endl;
    respeaker->Resume();
    return 0;
}
