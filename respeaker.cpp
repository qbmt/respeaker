#include <iostream>
#include <csignal>
#include <vector>
#include <thread>
#include "./src/kwsDirection.h"
#include "./src/beamformingRecorder.h"
#include "./src/gstt_transcribe.h"
#include "./src/mqtt.h"
#include "./src/resources/dotenv.h"

using namespace dotenv;
using namespace std;


static bool stop = false;

KwsDirection kwsDir;
BeamformingRecorder bfRec;
MqttClient mqttClient(env["MAC_ADDRESS"], "tcp://" + env["SERVER_ADDRESS"]);

void SignalHandler(int signal){
  cerr << "Caught signal " << signal << ", terminating..." << endl;
  
  kwsDir.stopKwsDir();
  kwsDir.stop = true;
  bfRec.stopBfRec();
  bfRec.stop = true;
}

void triggerMqttTread() {
    MqttClient mqttTriggerClient(env["MAC_ADDRESS"] + "trigger", "tcp://" + env["SERVER_ADDRESS"]);
    mqttTriggerClient.connectToBroker();
    mqttTriggerClient.triggerRespeakerViaMqtt("respeaker/config");
    BeamformingRecorder triggerBfRec;
    triggerBfRec.initializeRespeaker();
    triggerBfRec.startBfRec();
    triggerBfRec.pauseBfRec();

    while (true) {
        if(mqttTriggerClient.getTriggered()){
            triggerBfRec.resumeBfRec();
            triggerBfRec.initializeDefaults();
            Transcribe triggerTrans;
            triggerTrans.init();
            std::future<std::vector<std::string>> triggerResponse = std::async(std::launch::async, &Transcribe::ResponseThreadMain, &triggerTrans);
            triggerBfRec.record(&triggerTrans);
            std::vector<std::string> triggerGsttOutput = triggerResponse.get();
            triggerBfRec.pauseBfRec();
		    std::string triggerOutput = "";
        
            for (int i = 0; i < triggerGsttOutput.size(); i++) {
               triggerOutput += triggerGsttOutput[i] + " ";
            }
            mqttTriggerClient.publish("command/" + env["MAC_ADDRESS"], triggerOutput);
            mqttTriggerClient.setTriggered(false);
        }
    }
}

int main() {
    struct sigaction sig_int_handler;
    sig_int_handler.sa_handler = SignalHandler;
    sigemptyset(&sig_int_handler.sa_mask);
    sig_int_handler.sa_flags = 0;
    sigaction(SIGINT, &sig_int_handler, NULL);
    sigaction(SIGTERM, &sig_int_handler, NULL);
    
    std::thread triggerThread(triggerMqttTread);

    kwsDir.initializeRespeaker();
    bfRec.initializeRespeaker();
    int startKwsDir;
    int startBfRec;
    
    
    mqttClient.connectToBroker();
    
    startKwsDir = kwsDir.startKwsDir();
    kwsDir.pauseKwsDir();

    startBfRec = bfRec.startBfRec();
    bfRec.pauseBfRec();

    mqttClient.publish("leds/" + env["MAC_ADDRESS"], "ready");

    while (!stop) {
    
        if (!mqttClient.isConnected()) {
            cout << "not connected try to connect" << endl;
            mqttClient.connectToBroker();
	    }
        

        startKwsDir = kwsDir.resumeKwsDir();
        

        if (startKwsDir == -1) {
            break;
        }

        kwsDir.initializeDefaults();
        string mic = kwsDir.getMic();
        cout << "\n#################### the hotword was called from mic: " << mic << " ####################\n" << endl;
        kwsDir.pauseKwsDir();

        startBfRec = bfRec.resumeBfRec();

        if (startBfRec == -1) {
            break;
        }

        mqttClient.publish("leds/" + env["MAC_ADDRESS"], "KWS triggered");
        mqttClient.publish("status", "connected");
        bfRec.initializeDefaults();
        bfRec.setBeam(mic);
        Transcribe trans;
        trans.init();
        std::future<std::vector<std::string>> response = std::async(std::launch::async, &Transcribe::ResponseThreadMain, &trans);
        mqttClient.publish("leds/" + env["MAC_ADDRESS"], "listen" + mic);
        bfRec.record(&trans);
        mqttClient.publish("leds/" + env["MAC_ADDRESS"], "off");
        vector<string> gstt_output = response.get();
        bfRec.pauseBfRec();

        
        mqttClient.publish("leds/" + env["MAC_ADDRESS"], "think");
        string output = "";
        
        for (int i = 0; i < gstt_output.size(); i++) {
            output += gstt_output[i] + " ";
        }

        mqttClient.publish("command/" + env["MAC_ADDRESS"], output);
    }

    return 0;
}