# Installation Respeaker

## OS

Click to download the latest image zip files. We use the *lxqt* version so that's the version with desktopversion.  
We will use an sd card as installation media, so that's why we use *sd*.  
So the image you'll need is: [*respeaker-debian-9-lxqt-sd-20180801-4gb.img.xz*.](https://v  2.fangcloud.com/share/7395fd138a1cab496fd4792fe5?lang=en)  

### Bootable image

Use balena Etcher to make image bootable.

![balena etcher](img/balena_etcher.png)

### SSH

After balenaEtcher is done you have to open file manager and and a file called *ssh* without a file extension.  

![ssh file](img/ssh_file.png)

### Boot

Now you can put the sd card in the Respeaker Core V2.0 and put the power and ethernet cable in. After the device is booted you can acces the Respeaker via ssh.  

## Setup software

You can now acces the command line of the Respeaker update an upgrade it.

```bash
sudo apt update
sudo apt upgrade
```

### Git setup

Before you can clone the repository you have to install git.

```bash
sudo apt-get install git
```

Now you have to setup the git on the Respeaker.

```bash
sudo git config --global user.name "Your Name"
git config --global user.email email@example.com 
```

### Clone repository

```bash
git clone https://FrederikFeys@bitbucket.org/qbmt/respeaker.git
```

And install the necessary library's.

```bash
sudo apt install -y librespeaker-dev
sudo apt install -y libsndfile-dev
```

### Samba share

```bash
sudo apt-get install samba samba-common-bin
```

```bash
sudo nano /etc/samba/smb.conf
```

You can delete everything and paste following code.

```bash
[global]
netbios name = Respeaker
server string = The Respeaker File Center
workgroup = WORKGROUP

[Respeaker]
path = /home/respeaker
comment = No comment
writeable=Yes
create mask=0777
directory mask=0777
public=no
```

Add the user and give it a password.

```bash
sudo smbpasswd -a respeaker
```

Restart the service.

```bash
sudo service smbd restart
```

### Google STT

#### Preparation

Step 1

```bash
echo 'if [ -f ~/.bashrc ]; then' >> ~/.bash_profile
echo '      . ~/.bashrc' >> ~/.bash_profile
echo 'fi' >> ~/.bash_profile
```

Step 2
Put your credential file on the Respeaker and do the following, but please update the path

```bash
echo 'export GOOGLE_APPLICATION_CREDENTIALS=/path/to/your/credentials-key.json' >> ~/.bash_profile
```

#### Update Python

```bash
yes | sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget
curl -O https://www.python.org/ftp/python/3.8.1/Python-3.8.1.tar.xz
tar -xvf Python-3.8.1.tar.xz
cd Python-3.8.1
./configure --enable-optimizations
make -j4
sudo make altinstall
pip install --upgrade pip
```

#### Install CMAKE

```bash
sudo apt install cmake
```

update CMAKE

```bash
yes | sudo apt-get purge cmake
version=3.15
build=7
mkdir ~/temp
cd ~/temp
wget https://cmake.org/files/v$version/cmake-$version.$build.tar.gz
tar -xzvf cmake-$version.$build.tar.gz
cd cmake-$version.$build/
./bootstrap
make -j4
sudo make install
```

Reboot system

```bash
sudo reboot
```

Check CMAKE version

```bash
cmake --version
```

#### GRPC

If you have errors after 'cmake ../..', please fix them before going on. First run the command again, then you will see a clear output of what packages needs to be installed extra. After installing the packages, run the command a third time.

```bash
yes | sudo apt-get install build-essential autoconf libtool pkg-config cmake golang libunwind-dev
git clone -b v1.27.x https://github.com/grpc/grpc
cd grpc
git submodule update --init
mkdir -p cmake/build
cd cmake/build
cmake ../..
make
```

#### Googleapis

```bash
cd
git clone https://github.com/googleapis/googleapis.git googleapis/
cd googleapis
yes | sudo apt install python3-pip
sudo pip3 install virtualenv
sudo pip3.8 install googleapis-artman
configure-artman
```

Enter the path to the directory where you cloned googleapis in. (example: /home/respeaker/)

```bash
echo 'export LANGUAGE=cpp' >> ~/.bash_profile
```

#### Protobuf

Download the latest version of [protofub](https://github.com/protocolbuffers/protobuf/releases/tag/v3.11.4) (.tar.gz) and put it on your system.

```bash
cd
tar -xvf protobuf-cpp-3.11.4.tar.gz
cd protobuf-3.11.4
./configure
make
make check
sudo make install
sudo ldconfig
```

#### Go on installing Googleapis

```bash
cd ~/googleapis/
make all
echo 'export GOOGLEAPIS_GENS_PATH="$HOME/googleapis/gens"' >> ~/.bash_profile
```

Now reboot your system.

## MQTT

```bash
sudo apt-get install build-essential gcc make cmake cmake-gui cmake-curses-gui libssl-dev doxygen graphviz libcppunit-dev
```

Be warned: if the make command won't work, remove paho.mqtt.c and clone it again, but without checkout.

```bash
cd ~
git clone https://github.com/eclipse/paho.mqtt.c.git
cd paho.mqtt.c
git checkout v1.3.1
make -Bbuild -H. -DPAHO_ENABLE_TESTING=OFF
sudo cmake --build build/ --target install
cd build
sudo make
sudo make install
sudo ldconfig
```

```bash
cd ~
git clone https://github.com/eclipse/paho.mqtt.cpp
cd paho.mqtt.cpp
cmake -Bbuild -H. -DPAHO_BUILD_DOCUMENTATION=TRUE -DPAHO_BUILD_SAMPLES=TRUE
sudo cmake --build build/ --target install
cd build
sudo make
sudo make install
sudo ldconfig
```
